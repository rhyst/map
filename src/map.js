import * as React from "react";
import { Map as LeafletMap, TileLayer, CircleMarker } from "react-leaflet";
import Track from "./track";

class Map extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      lat: 51.505,
      lng: -0.09,
      zoom: 9,
    };
  }

  renderHighlightSegment = () => {
    if (this.props.highlightSegment) {
      const track = this.props.tracks.find(
        (t) => t.id === this.props.highlightSegment.id
      );
      const coordinates = track.geoJSON.features[
        this.props.highlightSegment.index
      ].geometry.coordinates[0]
        .slice()
        .reverse();
      return (
        <CircleMarker
          color="white"
          weight={2}
          radius={5}
          fillColor="#38ceff"
          fillOpacity={1}
          center={coordinates}
        />
      );
    }
    return null;
  };

  render() {
    const {
      tracks,
      selectedId,
      hoveredId,
      setHovered,
      setSelected,
    } = this.props;
    const position = [this.state.lat, this.state.lng];
    return (
      <LeafletMap
        onClick={(e, m, a) => {
          this.props.setSelected(null);
        }}
        center={position}
        zoom={this.state.zoom}
        bounds={this.props.bounds}
        zoomControl={false}
        preferCanvas={true}
      >
        <TileLayer
          attribution='&amp;copy <a href="http://osm.org/copyright">OpenStreetMap</a> contributors'
          url="https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png"
        />
        {tracks.map((t) => (
          <Track
            key={t.id}
            track={t}
            selected={selectedId === t.id}
            hovered={hoveredId === t.id}
            setHovered={setHovered}
            setSelected={setSelected}
          />
        ))}
        {this.renderHighlightSegment()}
      </LeafletMap>
    );
  }
}

export default Map;
