import * as React from "react";
import { GeoJSON, withLeaflet } from "react-leaflet";

class Track extends React.Component {
  geoJSONStyle = (feature) => {
    const { selected, hovered } = this.props;
    let color = "#f00";
    if (selected && !hovered) color = "#0f0";
    if (selected && hovered) color = "#9f9";
    if (!selected && hovered) color = "#f99";
    return {
      color,
      weight: 3,
      fillOpacity: 0.5,
      fillColor: "#f00",
    };
  };

  shouldComponentUpdate(nextProps) {
    if (this.props.tracks !== nextProps.tracks) return true;
    if (this.props.selected !== nextProps.selected) return true;
    if (this.props.hovered !== nextProps.hovered) return true;
    return false;
  }

  componentDidUpdate(prevProps) {
    if (!prevProps.selected && this.props.selected && this.layer) {
      this.layer.bringToFront();
    }
  }

  render() {
    if (!this.props.track) return null;
    return (
      <GeoJSON
        onadd={(e) => {
          this.layer = e.target;
        }}
        data={this.props.track.geoJSON}
        style={this.geoJSONStyle}
        onMouseOver={(e) =>
          this.props.setHovered(e.layer.feature.properties.id)
        }
        onMouseOut={() => this.props.setHovered(null)}
        onClick={(e) =>
          setTimeout(
            () => this.props.setSelected(e.layer.feature.properties.id),
            0
          )
        }
      />
    );
  }
}

export default Track;
