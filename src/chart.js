import ChartJS from "chart.js";
import * as React from "react";
import { throttle, max } from "lodash";

class Chart extends React.Component {
  constructor() {
    super();
    this.canvas = React.createRef();
    this.chart = null;

    const horizonalLinePlugin = {
      beforeDraw: (chartInstance) => {
        if (chartInstance.tooltip._view.opacity !== 0) {
          const ctx = chartInstance.chart.ctx;
          const horiz = chartInstance.tooltip._view.caretX;
          ctx.lineWidth = 1;
          ctx.beginPath();
          ctx.moveTo(horiz, chartInstance.chartArea.bottom);
          ctx.lineTo(horiz, chartInstance.chartArea.top);
          ctx.strokeStyle = "rgba(0, 0, 0, 0.5)";
          ctx.stroke();
        }
      },
      afterEvent: throttle(this.handleHover, 15),
    };
    ChartJS.pluginService.register(horizonalLinePlugin);
  }

  handleHover = (chartInstance, event) => {
    const chartIndex = chartInstance.getElementsAtXAxis(event)[0]._index;
    const index = chartInstance.data.datasets[0].data[chartIndex].i;
    this.props.setHighlightSegment(this.props.selectedTrack.id, index);
  };

  componentDidUpdate(prevProps) {
    if (prevProps.selectedTrack?.id === this.props.selectedTrack?.id) return;
    if (!this.canvas.current) return;
    const ctx = this.canvas.current.getContext("2d");
    if (this.chart) {
      this.chart.destroy();
      this.chart = null;
    }
    if (this.props.selectedTrack) {
      // Set a reasonable max number of data points
      const divisor = Math.ceil(
        this.props.selectedTrack.geoJSON.features.length / 2000
      );
      const data = this.props.selectedTrack.geoJSON.features
        .map((f, index) => ({ ...f.properties, index }))
        .filter((p) => p.index % divisor === 0);

      // Bullshit to get left/right y axes to line up
      const leftYMax = max(data.map((d) => d.elevation));
      const rightYMax = max(data.map((d) => d.speed));
      const amountOfLabels = 10;
      const newLeftYMax = calculateMax(amountOfLabels, leftYMax);
      const leftYStep = newLeftYMax / amountOfLabels;
      const newRightYMax = calculateMax(amountOfLabels, rightYMax);
      const rightYStep = newRightYMax / amountOfLabels;
      function calculateMax(amountOfLabels, max) {
        if (max % amountOfLabels === 0) {
          return max;
        }
        const diffDivisibleByAmountOfLabels =
          amountOfLabels - (max % amountOfLabels);
        return max + diffDivisibleByAmountOfLabels;
      }

      this.chart = new ChartJS(ctx, {
        type: "line",
        data: {
          datasets: [
            {
              yAxisID: "elevation",
              borderColor: "#f00",
              data: data.map((d) => ({
                y: d.elevation,
                x: d.cumulativeDistance,
                i: d.index,
              })),
              fill: true,
              pointRadius: 0,
              lineTension: 0.1,
            },
            {
              yAxisID: "speed",
              borderColor: "#0f0",
              data: data.map((d) => ({
                y: d.speed,
                x: d.cumulativeDistance,
                i: d.index,
              })),
              fill: true,
              pointRadius: 0,
              lineTension: 0.1,
            },
          ],
        },
        options: {
          title: {
            display: false,
          },
          legend: {
            display: false,
          },
          responsive: true,
          maintainAspectRatio: false,
          tooltips: {
            mode: "index",
            intersect: false,
            displayColors: false,
            callbacks: {
              title: () => {},
              beforeBody: () => {},
              beforeLabel: () => {},
              beforeTitle: () => {},
              label: (tooltipItem) =>
                tooltipItem.datasetIndex === 0
                  ? `${tooltipItem.yLabel}m`
                  : `${tooltipItem.yLabel}km/h`,
              labelTextColor: function (tooltipItem, chart) {
                return "rgb(255, 255, 255)";
              },
            },
          },
          scales: {
            yAxes: [
              {
                id: "elevation",
                scaleLabel: {
                  display: true,
                  labelString: "Height (m)",
                },
                ticks: {
                  beginAtZero: true,
                  max: newLeftYMax,
                  stepSize: leftYStep,
                  // suggestedMax: 150,
                },
              },
              {
                id: "speed",
                position: "right",
                scaleLabel: {
                  display: false,
                  labelString: "Speed (km/h)",
                },
                ticks: {
                  beginAtZero: true,
                  max: newRightYMax,
                  stepSize: rightYStep,
                },
              },
            ],
            xAxes: [
              {
                type: "linear",
                afterBuildTicks: (axis, ticks) => {
                  const last = ticks.length - 1;
                  const stepSize = ticks[2] - ticks[1];
                  if (ticks[last] - ticks[last - 1] < stepSize / 4) {
                    return [...ticks.slice(0, last)];
                  }
                  return ticks;
                },
                scaleLabel: {
                  display: false,
                },
                gridLines: {
                  display: false,
                },
                ticks: {
                  min: data[0].cumulativeDistance,
                  max: data[data.length - 1].cumulativeDistance,
                  display: true,
                  callback: (value, index, values) => {
                    return `${(value / 1000).toFixed(0)}km`;
                  },
                  maxRotation: 0,
                  minRotation: 0,
                  beginAtZero: false,
                },
              },
            ],
          },
        },
      });
    }
  }

  renderInfo = () => {
    const isSelected = !!this.props.selectedTrack;
    const distance = this.props.selectedTrack;
    return (
      <div className={`info ${!isSelected ? "out" : ""}`}>
        {isSelected ? (
          <>
            <h1>{this.props.selectedTrack.name}</h1>
            <div className="grid">
              <p>Type: </p>
              <p>
                {this.props.selectedTrack.type === "rides" ? "Cycle" : "Hike"}
              </p>
              <p>Average Speed:</p>
              <p>
                {this.props.selectedTrack.averageSpeed
                  ? this.props.selectedTrack.averageSpeed.toFixed(1)
                  : "?"}
                 km/h
              </p>
              <p>Distance:</p>
              <p>
                {(this.props.selectedTrack.totalDistance / 1000).toFixed(1)}
                 km
              </p>
              <p>Max Speed:</p>
              <p>
                {this.props.selectedTrack.maxSpeed
                  ? this.props.selectedTrack.maxSpeed
                  : "? "}
                 km/h
              </p>
              <p>Ascent:</p>
              <p>
                {Math.floor(this.props.selectedTrack.ascent)}
                 m
              </p>
              <p>Descent:</p>
              <p>
                {Math.floor(this.props.selectedTrack.descent)}
                 m
              </p>
            </div>
          </>
        ) : null}
      </div>
    );
  };

  render() {
    const isSelected = !!this.props.selectedTrack;
    return (
      <>
        {this.renderInfo()}
        <div id="chart-container" className={`${!isSelected ? "out" : ""}`}>
          <canvas ref={this.canvas}></canvas>
        </div>
      </>
    );
  }
}

export default Chart;
