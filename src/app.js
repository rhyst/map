import * as React from "react";
import { LatLng, LatLngBounds } from "leaflet";
import { LeafletProvider } from "react-leaflet";
import omnivore from "leaflet-omnivore";
import uniqid from "uniqid";
import { extent } from "geojson-bounds";
import { max } from "lodash";
import Map from "./map";
import Chart from "./chart";
import { parseQuery } from "./util";

const query = parseQuery(window.location.search);

const SPEED_THRESHOLD = 3;

class App extends React.Component {
  constructor() {
    super();
    this.state = {
      tracks: [],
      selectedId: null,
      hoveredId: null,
      highlightSegment: null,
      bounds: null,
    };
  }

  setSelected = (selectedId) => {
    window.history.pushState(
      null,
      null,
      selectedId
        ? `?track=${
            this.state.tracks.find((t) => t.id === selectedId).filename
          }`
        : "?"
    );
    this.setState({ selectedId, highlightSegment: null });
  };

  setHovered = (hoveredId) => this.setState({ hoveredId });

  setHighlightSegment = (id, index) => {
    this.setState({ highlightSegment: id && index ? { id, index } : null });
  };

  async fetch() {
    let allBounds = new LatLngBounds();
    const getTrack = (filename) =>
      new Promise((resolve) =>
        omnivore.geojson(`${filename}`, null).on("ready", (e) => {
          const geoJSON = e.target.toGeoJSON();
          const id = uniqid();
          const type = filename.split("/")[0];
          const name = filename
            .split("/")
            .pop()
            .split(".")[0]
            .replace(/_/g, " ")
            .replace(/^\d*\s/, "");
          // Distance
          const totalDistance = geoJSON.features.slice(-1).pop().properties
            .cumulativeDistance;
          geoJSON.features.forEach((f, index) => {
            f.properties.id = id;
          });
          // Speed
          const maxSpeed = max(geoJSON.features.map((f) => f.properties.speed));
          const movingTime = geoJSON.features[0].properties.timeFrom
            ? geoJSON.features
                .filter((f) => f.properties.speed > 4)
                .reduce((p, c) => {
                  return p + (c.properties.timeTo - c.properties.timeFrom);
                }, 0)
            : null;
          const averageSpeed = movingTime
            ? (totalDistance / movingTime) * 3.6
            : null;
          // Elevation
          let ascent = 0;
          let descent = 0;
          geoJSON.features.forEach((f, i) => {
            if (i === 0) return 0;
            const diff =
              geoJSON.features[i].properties.elevation -
              geoJSON.features[i - 1].properties.elevation;
            if (diff > 0) ascent += diff;
            if (diff < 0) descent += diff;
          });
          const ext = extent(geoJSON);
          const bounds = new LatLngBounds(
            new LatLng(ext[1], ext[0]),
            new LatLng(ext[3], ext[2])
          );
          allBounds.extend(bounds);
          resolve({
            filename,
            id,
            geoJSON,
            bounds,
            type,
            name,
            totalDistance,
            maxSpeed,
            averageSpeed,
            ascent,
            descent,
          });
        })
      );
    const response = await fetch("tracks.json", {
      method: "get",
    });
    const trackList = await response.json();
    const tracks = await Promise.all(
      trackList.filter((t) => !t.includes("tracks.json")).map(getTrack)
    );
    this.setState({ tracks });
    if (query.track) {
      const track = tracks.find((t) => t.filename === query.track);
      if (track) {
        this.setState({
          selectedId: track.id,
          bounds: track.bounds,
        });
      }
    } else {
      this.setState({ tracks, bounds: allBounds });
    }
  }

  componentDidMount() {
    this.fetch();
  }

  render() {
    return (
      <>
        <Map
          tracks={this.state.tracks}
          selectedId={this.state.selectedId}
          hoveredId={this.state.hoveredId}
          highlightSegment={this.state.highlightSegment}
          bounds={this.state.bounds}
          setSelected={this.setSelected}
          setHovered={this.setHovered}
        />
        <Chart
          selectedTrack={this.state.tracks.find(
            (t) => t.id === this.state.selectedId
          )}
          setHighlightSegment={this.setHighlightSegment}
        />
      </>
    );
  }
}

export default App;
