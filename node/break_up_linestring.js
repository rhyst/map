const fs = require("fs");

const file = fs.readFileSync(process.argv[2], { encoding: "utf-8" });

const geojson = JSON.parse(file);

const getDistance = ([lat1, lon1], [lat2, lon2]) => {
  if (lat1 == lat2 && lon1 == lon2) {
    return 0;
  } else {
    var radlat1 = (Math.PI * lat1) / 180;
    var radlat2 = (Math.PI * lat2) / 180;
    var theta = lon1 - lon2;
    var radtheta = (Math.PI * theta) / 180;
    var dist =
      Math.sin(radlat1) * Math.sin(radlat2) +
      Math.cos(radlat1) * Math.cos(radlat2) * Math.cos(radtheta);
    if (dist > 1) {
      dist = 1;
    }
    dist = Math.acos(dist);
    dist = (dist * 180) / Math.PI;
    dist = dist * 60 * 1.1515 * 1.609344 * 1000;
    return dist;
  }
};

const features = [];

for (let i = 0; i < geojson.features.length; i++) {
  for (
    let j = 0;
    j < geojson.features[i].geometry.coordinates.length - 1;
    j++
  ) {
    const coords = geojson.features[i].geometry.coordinates[j];
    const nextCoords = geojson.features[i].geometry.coordinates[j + 1];
    const elevation = coords[2] || 0;
    const nextElevation = nextCoords[2] || 0;
    const averageElevation = (elevation + nextElevation) / 2;
    const distance = Math.floor(getDistance(coords, nextCoords));
    const slope = Math.floor(100 * ((nextElevation - elevation) / distance));
    features.push({
      type: "Feature",
      geometry: {
        type: "LineString",
        coordinates: [coords.slice(0, 2), nextCoords.slice(0, 2)],
      },
      properties: {
        elevation: averageElevation,
        slope: slope,
        speed: 4,
      },
    });
  }
}

const collection = {
  type: "FeatureCollection",
  features: features,
};

fs.writeFileSync(process.argv[2] + ".2", JSON.stringify(collection), {
  encoding: "utf-8",
});
