const glob = require("glob");
const fs = require("fs");

// options is optional
glob("**/*.json", { cwd: "data" }, function (er, files) {
  console.log(`Found ${files.length} files`);
  fs.writeFileSync("data/tracks.json", JSON.stringify(files));
});
