const fs = require("fs");

const file = fs.readFileSync(process.argv[2], { encoding: "utf-8" });

const geojson = JSON.parse(file);

function getDistance(coords1, coords2) {
  function toRad(x) {
    return (x * Math.PI) / 180;
  }
  var lon1 = coords1[0];
  var lat1 = coords1[1];
  var lon2 = coords2[0];
  var lat2 = coords2[1];
  var R = 6371000; // m
  var x1 = lat2 - lat1;
  var dLat = toRad(x1);
  var x2 = lon2 - lon1;
  var dLon = toRad(x2);
  var a =
    Math.sin(dLat / 2) * Math.sin(dLat / 2) +
    Math.cos(toRad(lat1)) *
      Math.cos(toRad(lat2)) *
      Math.sin(dLon / 2) *
      Math.sin(dLon / 2);
  var c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
  var d = R * c;
  return d;
}

const min = 200;
const sensitive_coords = [-2.530851, 52.716007];

console.log(geojson.features.length);

for (let i = geojson.features.length - 1; i > -1; i--) {
  const distance1 = getDistance(
    geojson.features[i].geometry.coordinates[0],
    sensitive_coords
  );
  const distance2 = getDistance(
    geojson.features[i].geometry.coordinates[1],
    sensitive_coords
  );
  if (distance1 < min || distance2 < min) {
    geojson.features.splice(i, 1);
  }
}

const low = geojson.features[0].properties.cumulativeDistance;
for (let i = geojson.features.length - 1; i > -1; i--) {
  geojson.features[i].properties.cumulativeDistance =
    geojson.features[i].properties.cumulativeDistance - low;
}

console.log(geojson.features.length);

fs.writeFileSync(process.argv[2], JSON.stringify(geojson), {
  encoding: "utf-8",
});
